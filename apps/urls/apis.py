from django.urls import path

from apps.controllers import project, abn

urlpatterns = [
    path('request/<int:id>', project.api_get_project,
         name='api.request.show'),
    path('request/<int:id>/close', project.close_request,
         name='api.request.close'),
    path('abn/verify', abn.verify_abn,
         name='api.abn.verify')
]
