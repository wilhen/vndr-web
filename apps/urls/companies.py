from django.urls import path, include

from apps.controllers.company import create_company_profile, step_management, email_verification, show_company_profile, get_companies, select_company

urlpatterns = [
    path('create', create_company_profile, name="company.create"),
    path('step', step_management, name="company.step"),
    path('change', get_companies, name="company.change"),
    path('<int:id>/change', select_company, name="company.select"),
    path('verify/<str:verification_id>',
         email_verification, name="company.verify.email"),
    path('<int:id>',
         show_company_profile, name="company.show")
]
