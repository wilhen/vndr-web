from django.urls import path

from apps.controllers import offer

urlpatterns = [
    path('', offer.index, name='offer.index'),
    path('<int:id>', offer.show, name='offer.show'),
    path('<int:id>/view', offer.show_index, name='offer.show.index'),
    path('<int:id>/version', offer.add_version, name='offer.version.add'),
    path('store', offer.store, name='offer.store'),
    path('<int:id>/delete', offer.delete_offer, name='offer.delete'),
    path('<int:id>/status', offer.accept_or_reject_offer,
         name='offer.update.status')
]
