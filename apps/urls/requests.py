from django.urls import path

from apps.controllers import project


urlpatterns = [
    path('', project.index, name='app.index'),
    path('request/<int:id>', project.get_project, name='request.show'),
    path('request/create', project.get_create_project,
         name='request.create'),
    path('request/store', project.store_project,
         name='request.store'),
    path('requests', project.list_projects,
         name='request.index'),
    path('request/<int:id>/edit', project.edit_project,
         name='request.edit'),
    path('request/<int:id>/update', project.update_project,
         name='request.update'),
    path('request/<int:id>/delete', project.delete_request,
         name='request.delete')
]
