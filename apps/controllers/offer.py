from datetime import datetime
from django.shortcuts import render, get_object_or_404
from django.http import  JsonResponse
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required

from rest_framework.decorators import api_view
from rest_framework.response import Response

from apps.models.offer import Offer
from apps.models.offer_version import OfferVersion
from apps.models.offer_status import OfferStatus
from apps.models.project import Project
from apps.helpers.close import is_closed

from apps.serializers.OfferVersionSerializer import OfferVersionSerializer
from apps.serializers.OfferSerializer import OfferSerializer
# Create your views here.


@require_http_methods(['GET'])
def show(request, id):
    offer = get_object_or_404(Offer, pk=id)

    context = {
        'offer': offer,
        'versions': offer.offerversion_set.all()
    }
    return render(request, 'frontend/offers/view.html', context)


@require_http_methods(['GET'])
def show_index(request, id):
    offer = get_object_or_404(Offer, pk=id)

    context = {
        'offer': offer,
        'versions': offer.offerversion_set.all()
    }
    return render(request, 'frontend/offers/view.index.html', context)


@require_http_methods(['GET'])
def index(request):
    offers = Offer.objects.filter(
        user_id=request.user.id).order_by('-created_at')

    return render(request, 'frontend/offers/index.html', {'offers': offers})


@login_required
@api_view(['POST'])
def store(request):

    data = request.data

    project = get_object_or_404(Project, pk=data['project_id'])

    if not can_create_offer(project, request.user.id):
        return Response({'message': 'User has active offer'}, status=432)

    if is_closed(project):
        return Response({'message': 'Request closed'}, status=432)

    form = OfferSerializer(data=data)
    if form.is_valid():

        offer = Offer(
            bid_price=data['bid_price'],
            user_id=request.user.id,
            project=project,
            status=OfferStatus.objects.get(pk=4)  # sent
        )
        offer.save()

        if offer:
            version = OfferVersion(
                user_id=offer.user_id,
                bid_price=offer.bid_price,
                offer=offer
            )
            version.save()
            # save to current version column
            offer.current_version = version.id
            offer.save()
            return Response({'message': 'Offer Created'}, status=201)
        else:
            return Response({'message': 'Offer cannot be created'}, status=410)

    return Response(form.errors, status=422)


@login_required
@require_http_methods(['DELETE'])
def delete_offer(request, id):

    offer = get_object_or_404(Offer, pk=id)

    if is_closed(offer.project):
        return JsonResponse({'message': 'Request Closed'}, status=430)

    if not is_offer_modifiable(offer):
        return JsonResponse({'message': 'Offer has been processed'}, status=430)

    offer.delete()
    return JsonResponse({'message': 'Offer Retracted'}, status=201)


@login_required
@api_view(['POST'])
def add_version(request, id):

    offer = get_object_or_404(Offer, pk=id)

    if is_closed(offer.project):
        return JsonResponse({'message': 'Request Closed'}, status=430)

    if not is_offer_modifiable(offer):
        return Response({'message': 'Offer has been processed'}, status=430)

    # data is to retrieve the JSON data
    data = request.data

    # validate JSON values with drf serializer
    form = OfferVersionSerializer(data=data)
    # check if valid
    if form.is_valid():

        version = OfferVersion(
            bid_price=data['bid_price'],
            offer=offer,
            user_id=request.user.id
        )
        version.save()

        if version:
            offer.status = OfferStatus.objects.get(pk=8)
            offer.bid_price = version.bid_price
            offer.current_version = version.id
            offer.save()
            serializer = OfferVersionSerializer(version, many=False)
            return Response(serializer.data, status=201)
        else:
            return Response({'message': 'Version cannot be created'}, status=410)
    else:
        return Response(form.errors, status=422)


@login_required
@api_view(['PUT'])
def accept_or_reject_offer(request, id):
    data = request.data

    if data['status'] != 3 and data['status'] != 5:
        return Response(status=422)

    offer = get_object_or_404(Offer, pk=id)
    offer.status = OfferStatus.objects.get(
        pk=data['status'])  # 3 == accepted || 5 == rejected

    if not is_offer_modifiable(offer):
        Response({'message': 'Offer has been processed'}, status=430)

    if is_closed(offer.project):
        return JsonResponse({'message': 'Request Closed'}, status=430)

    if data['status'] == 3:
        offer.is_accepted = True
        offer.accepted_at = datetime.now()
    else:
        offer.is_accepted = False

    offer.save()

    return Response({'message': 'Offer accepted'}, status=201)


# helper methods
def is_offer_modifiable(offer):
    # to check if user can still add a new version or retract an offer
    # once it is accepted => user should not able to retract or add new version
    # same case if it is deleted

    if offer.status.id == 3 or offer.status.id == 5:
        return False
    return True


def can_create_offer(project, user_id):

    # find an offer subset of project that the user has submitted in the past
    # which are not rejected
    return len(project.offer_set.filter(user_id=user_id).exclude(status__id=5)) == 0
