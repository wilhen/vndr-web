import requests
import json

from django.shortcuts import get_object_or_404
from django.core.mail import EmailMessage
from django.conf import settings
from django.views.decorators.http import require_http_methods

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser

from apps.models.company import Company
from django.shortcuts import render, redirect, get_object_or_404, reverse


@api_view(['GET'])
def verify_abn(request):
    # needs to load the data from company table
    payload = {'guid': settings.ABR_TOOL_GUID,
               'abn': '25617292869', 'name': 'two mates media'}
    r = requests.get(
        'https://abr.business.gov.au/json/MatchingNames.aspx', params=payload)

    # jsonp parsing
    jsonp = str(r.content)
    jsonp = jsonp.split('callback(')[1]
    jsonp = jsonp.split(')')[0]

    response = json.loads(jsonp)

    names = response['Names']
    for name in names:
        # 1. find the based on the name;
        # 2. match the business that has the number, registered under the state;
        # 3. and check the status
        if name['Abn'] == payload['abn'] and int(name['AbnStatus']) == 1 and (name['State']).lower() == ('qld').lower():

            return Response({'message': 'ok'})

    return Response({'message': 'abn not found'}, status=404)
