import json
import redis

from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.template import loader
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage
from django.utils.crypto import get_random_string
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.conf import settings
from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser

from apps.models.company import Company
from apps.models.company_email_verifications import CompanyEmailVerification
from apps.serializers.Request.ApiCompanySerializer import ApiCompanySerializer
from apps.serializers.Request.ApiLocationSerializer import ApiLocationSerializer


r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)
parser_classes = (MultiPartParser, )


@login_required
@require_http_methods(['GET'])
def create_company_profile(request):
    apiKey = settings.GOOGLE_PLACES_API_KEY
    return render(request, 'frontend/companies/create.html', context={'apiKey': apiKey})


@login_required
@require_http_methods(['GET'])
def get_companies(request):
    companies = request.user.companies.order_by('id')
    return render(request, 'frontend/companies/select.html', context={'companies': companies.all()})


@login_required
@require_http_methods(['POST'])
def select_company(request, id):

    company = get_object_or_404(Company, pk=id)
    # store in redis
    request.session['company_id'] = company.id
    r.set('company_' + str(request.user.id), company.id)
    return JsonResponse({'message': 'Company Changed'}, status=201)


@login_required
@api_view(['GET', 'POST'])
def step_management(request):
    if request.method == "GET":
        data = json.loads(r.get(request.user.id)) if r.get(
            request.user.id) else None
        return JsonResponse({'data': data}, status=200)

    data = request.data

    if int(data['step']) == 1:

        data = data.dict()

        form = ApiCompanySerializer(data=data)
        if not form.is_valid():
            return Response(form.errors, status=422)

        if 'company_logo' in request.FILES:
            # upload files implementation
            form_obj = request.FILES['company_logo']
            # defaults to  MEDIA_ROOT

            fs = FileSystemStorage(location=settings.MEDIA_URL)
            filename = fs.save(form_obj.name, form_obj)
            file_url = fs.url(filename)

            # replace company_logo to generated file_url
            data['company_logo'] = file_url
        else:
            data['company_logo'] = None

        r.set(request.user.id, json.dumps(data))
        return Response(data, status=201)

    elif int(data['step']) == 2:

        form = ApiLocationSerializer(data=data)
        if not form.is_valid():
            return Response(form.errors, status=422)
        company_partial = json.loads(r.get(request.user.id))

        company = Company(
            company_name=company_partial['name'],
            registered_business_name=company_partial['rbn'],
            abn=company_partial['abn'],
            phone_number=company_partial['phone'],
            business_email=company_partial['email'],
            fax_number=company_partial['fax'] if company_partial['fax'] else None,
            website=company_partial['website'] if company_partial['website'] else None,
            company_logo=company_partial['company_logo'] if company_partial['company_logo'] else None,
            address=data['address'],
            postcode=int(data['postcode']),
            state=data['state'],
            suburb=data['suburb'],
            country=data['country'],
            lat=data['lat'] if data['lat'] else None,
            lng=data['lng'] if data['lng'] else None,
            user_id=request.user.id
        )

        company.save()

        if company:
            # delete key in redis
            r.delete(request.user.id)

            verification_id = get_random_string(length=32)

            message = loader.get_template(
                'emails/business_email_verification.html').render({'verification_id': verification_id})

            # send verification email
            mail = EmailMessage(
                subject='Verify your Business Email',
                body=message,
                reply_to=['noreply@haiku.com'],
                from_email='system@haiku.com',
                to=[company.business_email]
            )

            mail.content_subtype = 'html'
            mail.send()

            verification = CompanyEmailVerification(
                company_id=company.id,
                is_accepted=False,
                verification_id=verification_id
            )
            verification.save()

        return Response({'message': 'Company Profile Created', 'id': company.id}, status=201)

    return Response({'message': 'Step Invalid'}, status=400)


@login_required
@require_http_methods(['GET'])
def email_verification(request, verification_id):
    # need email address
    # verification_id is unique;
    # every time the email sent again for a company the token is regenerated
    verification = get_object_or_404(
        CompanyEmailVerification, verification_id=verification_id)

    if not verification.is_accepted:
        verification.is_accepted = True
        verification.save()
        messages.success(request, 'Email Verified')
    else:
        messages.error(request, 'Invalid Verification')

    return redirect(reverse('company.show', kwargs={'id': verification.company_id}))


def resend_email_verification(request, company_id):
    pass


def store_company_profile(request):
    pass


def show_company_profile(request, id):
    company = get_object_or_404(Company, pk=id)
    mapbox_key = settings.MAPBOX_KEY
    return render(request, 'frontend/companies/show.html', context={'company': company, 'MAPBOX_KEY': mapbox_key})


def edit_company_profile(request, id):
    pass


def update_company_profile(request, id):
    pass


def delete_company_profile(request, id):
    pass
