from datetime import datetime
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from apps.helpers.close import is_closed
from apps.models.project import Project
from apps.models.contract_type import ContractType
from apps.models.offer_status import OfferStatus
from apps.serializers.ProjectSerializer import ProjectSerializer
from apps.serializers.Request.ApiProjectSerializer import ApiProjectSerializer

# Create your views here.


@require_http_methods(['GET'])
def index(request):
    projects = Project.objects.filter(close_at=None)
    context = {
        'projects': projects
    }

    return render(request, 'frontend/index.html', context)


@require_http_methods(['GET'])
def get_project(request, id):

    project = get_object_or_404(Project, pk=id)

    context = {
        'project': project,
        'offer':  project.offer_set.filter(user_id=request.user.id)[0] if has_offer(project, request.user.id) else None
    }

    return render(request, 'frontend/projects/view.html', context)


@api_view(['GET'])
def api_get_project(request, id):

    project = get_object_or_404(Project, pk=id)
    project.offer_set.all()

    serializer = ProjectSerializer(project, many=False)
    return Response(serializer.data)


@login_required
@require_http_methods(['GET'])
def get_create_project(request):
    contracts = ContractType.objects.all()

    return render(request, 'frontend/projects/create.html', {'contracts': contracts})


@login_required
@require_http_methods(['GET'])
def list_projects(request):
    flash = False

    storage = messages.get_messages(request)
    for message in storage:
        if message.extra_tags == '433':
            flash = True
            flash_message = message
            break

    projects = Project.objects.filter(project_poster_id=request.user.id)
    context = {
        'projects': projects,
        'error':  {"message": flash_message} if flash else None
    }

    return render(request, 'frontend/projects/index.html', context)


@login_required
@api_view(['POST'])
def store_project(request):
    # data is to retrieve the JSON data
    data = request.data

    # drf required false
    # but it cannot validate empty value

    # validate JSON values with drf serializer
    form = ApiProjectSerializer(data=data)
    if not form.is_valid():
        return Response(form.errors, status=422)

    else:
        project = Project(
            name=data['name'],
            description=data['description'],
            additional_requirements=data['additional_requirements'] if data['additional_requirements'] else None,

            location='Brisbane, Australia',
            icon_filepath='http://via.placeholder.com/100x100',
            min_price=data['min_price'] if data['min_price'] else None,
            max_price=data['max_price'],
            quantity=data['quantity'] if data['quantity'] else None,
            unit_of_measurement=data['unit_of_measurement'] if data['unit_of_measurement'] else None,
            service_coverage=data['service_coverage'] if data['service_coverage'] else None,
            contract_id=data['contract_type'],

            start_from=data['start_from'] if data['start_from'] else None,
            url=data['url'] if data['url'] else None,
            project_poster_id=request.user.id
        )
        project.save()

        return Response({'message': 'Request Submitted', 'request_id': project.id}, status=201)


@login_required
@require_http_methods(['GET'])
def edit_project(request, id):

    project = get_object_or_404(Project, pk=id)

    if is_closed(project):
        messages.add_message(request, messages.ERROR,
                             'Request Closed', extra_tags='433')
        return HttpResponseRedirect("/requests")

    if has_offer(project, accepted=True):
        messages.add_message(request, messages.ERROR,
                             'Has active offers', extra_tags='433')
        return HttpResponseRedirect("/requests")

    contracts = ContractType.objects.all()
    context = {
        'project': project,
        'contracts': contracts
    }
    return render(request, 'frontend/projects/edit.html', context)


@login_required
@api_view(['PUT'])
def update_project(request, id):
    # if there's an offer that is accepted
    # prevent delete or edit
    project = get_object_or_404(Project, pk=id)

    if is_closed(project):
        return JsonResponse({'message': 'Request Closed'}, status=430)

    if has_offer(project, accepted=True):
        return Response({'message': 'Has active offers'}, status=430)

    form = ApiProjectSerializer(data=request.data)

    # project.update(**request.POST)
    if not form.is_valid():
        return Response(form.errors, status=422)
    else:
        # save update
        # trick drf manipulation
        # save as python dictionary
        data = dict(form.data)
        # contract == contract_type
        data['contract'] = data['contract_type']
        # remove contract_type
        # since bulk update require match fields
        del data['contract_type']

        # bulk update instead of fill up each field
        project_set = Project.objects.filter(pk=id)
        project = project_set.update(**data)
        return Response(data, status=201)


@login_required
@require_http_methods(['DELETE'])
def delete_request(request, id):
    # if there's an offer that is accepted
    # prevent delete or edit

    project = get_object_or_404(Project, pk=id)

    if is_closed(project):
        return JsonResponse({'message': 'Request Closed'}, status=430)

    if has_offer(project, accepted=True):
        return JsonResponse({'message': 'Has active offers'}, status=430)
    project.delete()

    return JsonResponse({"message": "Request has been cancelled"}, status=201)


@login_required
@require_http_methods(['PUT'])
def close_request(request, id):

    project = get_object_or_404(Project, pk=id)
    if is_closed(project):
        return JsonResponse({'message': 'Request Closed'}, status=430)

    if not has_offer(project, accepted=True):
        return JsonResponse({'message': 'Need to have accepted offers'}, status=430)

    project.close_at = datetime.now()
    project.save()

    for offer in project.offer_set.exclude(status__id=3):
        offer.status = OfferStatus.objects.get(pk=5)
        offer.save()

    return JsonResponse({"message": "Request has been closed"}, status=201)


# helper method to check if user has sent an offer
# for a request
def has_offer(project, userId=None, accepted=False):

    # check if a user has an active offer
    if userId:
        offers = project.offer_set.filter(user_id=userId)
        if not accepted:
            # active offer scenario for a user
            # exclude declined offer
            offers = offers.exclude(status__id=5)  # 5 is declined offer
            return len(offers) > 0
        else:
            # accepted offer for a user
            offers = offers.filter(status__id=3)  # 3 is accepted offer
            # if there's one accepted offer
            # request shouldn't be alterable
            return len(offers) > 0
    else:
        if accepted:
            # accepted offer in a set
            offers = project.offer_set.filter(status__id=3)

            return len(offers) > 0
        # all other cases
        return False
