import redis
from django.conf import settings

from apps.models.company import Company

r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)


def company_processor(request):

    if request.user.is_authenticated and r.get('company_' + str(request.user.id)):

        company = Company.objects.get(
            pk=request.session['company_id'] if request.session.get('company_id', None) else r.get('company_' + str(request.user.id)))
        return {'selected_company': company}
    else:
        return {'selected_company': None}
