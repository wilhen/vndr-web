import Loading from "vue-loading-overlay";
import axios from "axios";
import app from "./app.js";

export default new Vue({
  el: "#create-offer",
  data() {
    return { bid_price: "", isLoading: false };
  },
  components: {
    loading: Loading
  },
  methods: {
    submitOffer(requestId) {
      this.isLoading = true;
      axios
        .post("/offer/store", {
          bid_price: this.bid_price,
          project_id: requestId
        })
        .then(response => {
          console.log(response);
          this.isLoading = false;
          swal({
            title: "Offer Created",
            text: "",
            icon: "success",
            closeOnClickOutside: false
          }).then(res => {
            if (res) {
              location.href = "/offer";
            }
          });
        })
        .catch(err => {
          this.isLoading = false;
          if (err.response.status == 432) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
          console.error(err);
        });
    }
  },
  mounted() {}
});
