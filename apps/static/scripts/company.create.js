import ProgressTracker from "progress-tracker";
import axios from "axios";
import app from "./app.js";
import Loading from "vue-loading-overlay";
import VueGoogleAutocomplete from "vue-google-autocomplete";

export default new Vue({
  //   component: ProgressTracker,
  el: "#company-create-form",
  components: {
    loading: Loading,
    VueGoogleAutocomplete: VueGoogleAutocomplete
  },
  data() {
    return {
      isLoading: false,
      step: {
        company: true,
        company_completed: false,
        location: false
      },
      errors: [],
      company: {
        name: "",
        rbn: "",
        abn: "",
        phone: "",
        fax: "",
        email: "",
        website: "",
        company_logo: ""
      },
      address: {
        address: "",
        postcode: "",
        suburb: "",
        state: "",
        country: "",
        lat: "",
        lng: ""
      }
    };
  },
  methods: {
    changeStep(step) {
      console.log(step);
      if (step == "company") {
        this.step.company = true;
        this.step.location = false;
      } else {
        if (this.step.company_completed) {
          this.step.location = true;
          this.step.company = false;
        }
      }
    },
    getAddressData(addressData, placeResultData, id) {
      this.address.address = placeResultData.formatted_address;
      this.address.country = addressData.country;
      this.address.lat = addressData.latitude;
      this.address.lng = addressData.longitude;
      this.address.suburb = addressData.locality;
      this.address.state = addressData.administrative_area_level_1;
      this.address.postcode = addressData.postal_code;
    },
    handleFileUpload() {
      console.log(this.$refs.file.files);
      this.company.company_logo = this.$refs.file.files[0];
    },
    submitStep() {
      this.isLoading = true;
      if (this.step.company && !this.step.location) {
        let formData = new FormData();
        formData.append("company_logo", this.company.company_logo);
        formData.append("step", 1);
        formData.append("name", this.company.name);
        formData.append("rbn", this.company.rbn);
        formData.append("abn", this.company.abn);
        formData.append("phone", this.company.phone);
        formData.append("fax", this.company.fax);
        formData.append("email", this.company.email);
        formData.append("website", this.company.website);

        axios
          .post("/company/step", formData, {
            headers: {
              "Content-Type": "multipart/form-data"
            }
          })
          .then(res => {
            console.log(res);
            this.step.company = false;
            this.step.location = true;
            this.errors = [];
            this.step.company_completed = true;
            this.isLoading = false;
          })
          .catch(err => {
            if ((err.response.status = 422)) {
              let errors = err.response.data;
              this.errors = errors;
            }
            this.isLoading = false;
            console.log(err.response);
          });
      } else {
        axios
          .post("/company/step", {
            step: 2,
            address: this.address.address,
            postcode: this.address.postcode,
            suburb: this.address.suburb,
            state: this.address.state,
            country: this.address.country,
            lng: this.address.lng ? this.address.lng : null,
            lat: this.address.lat ? this.address.lat : null
          })
          .then(res => {
            this.isLoading = false;
            console.log(res);
            location.href = "/company/" + res.data.id;
            // redirect to somewhere;
          })
          .catch(err => {
            console.log(err.response);
            this.isLoading = false;
            if ((err.response.status = 422)) {
              let errors = err.response.data;
              this.errors = errors;
            }
          });
      }
    },
    getLocation() {
      axios
        .get(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Amoeba&types=establishment&location=37.76999,-122.44696&radius=500&key=AIzaSyC6ohzvBe-vb5D0n5RvkElfNDo_dHD2o2I"
        )
        .then(res => {
          console.log(res);
        })
        .catch(err => {
          console.log(err.response);
        });
    },
    back() {
      window.history.back();
    }
  },
  created() {
    axios
      .get("/company/step")
      .then(res => {
        console.log(res);
        if (res.data.data) {
          swal({
            title: "Continue",
            text: "You have in progress company data",
            icon: "warning",
            buttons: ["Create New", true]
          }).then(result => {
            if (result) {
              this.company.name = res.data.data.name;
              this.company.abn = res.data.data.abn;
              this.company.rbn = res.data.data.rbn;
              this.company.phone = res.data.data.phone;
              this.company.fax = res.data.data.fax;
              this.company.email = res.data.data.email;
              this.company.website = res.data.data.website;

              // change step
              this.step.company_completed = true;
              this.step.location = true;
              this.step.company = false;
            }
          });
        }
      })
      .catch(err => {
        console.error(err);
      });
  }
});
