import axios from "axios";
import app from "./app.js";
import Loading from "vue-loading-overlay";

new Vue({
  el: "#offer-side",
  data() {
    return {
      selectedPage: "offer",
      isLoading: false
    };
  },
  components: {
    loading: Loading
  },
  methods: {
    page(pageNumber) {
      if (pageNumber == 1) {
        this.selectedPage = "offer";
      } else if (pageNumber == 2) {
        this.selectedPage = "version";
      } else {
        this.selectedPage = "discussion";
      }
    },
    acceptOffer(id) {
      this.isLoading = true;
      axios
        .put("/offer/" + id + "/status", { status: 3 })
        .then(res => {
          swal({
            title: "Offer Accepted",
            icon: "success",
            closeOnClickOutside: false
          }).then(() => {
            location.reload();
          });
        })
        .catch(err => {
          this.isLoading = false;
          console.log(err);
          if (err.response.status == 430) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
        });
    },
    rejectOffer(id) {
      this.isLoading = true;
      axios
        .put("/offer/" + id + "/status", { status: 5 })
        .then(res => {
          swal({
            title: "Offer Rejected",
            icon: "success",
            closeOnClickOutside: false
          }).then(() => {
            location.reload();
          });
        })
        .catch(err => {
          console.log(err);
          this.isLoading = false;
          if (err.response.status == 430) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
        });
    }
  },
  mounted() {}
});
