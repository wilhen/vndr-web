import Loading from "vue-loading-overlay";
import axios from "axios";
import app from "./app.js";

export default new Vue({
  el: "#update_request_form",
  props: {
    name: String,
    description: String,
    url: String,
    min_price: Number,
    max_price: Number,
    quantity: Number,
    unit_of_measurement: String,
    start_from: Date,
    contract_type: Number,
    service_coverage: String,
    additional_requirements: String
  },
  components: {
    loading: Loading
  },
  data() {
    return {
      errors: {},
      isLoading: false
    };
  },
  methods: {
    editForm(id, event) {
      event.preventDefault();
      this.isLoading = true;
      axios
        .put("/request/" + id + "/update", {
          name: this.name,
          description: this.description,
          url: this.url,
          min_price: this.min_price ? this.min_price : null,
          max_price: this.max_price,
          quantity: this.quantity ? this.quantity : null,
          unit_of_measurement: this.unit_of_measurement,
          start_from: this.start_from ? this.start_from : null,
          contract_type: this.contract_type,
          service_coverage: this.service_coverage,
          additional_requirements: this.additional_requirements
        })
        .then(res => {
          location.href = "/requests";
        })
        .catch(err => {
          console.log(err.response);
          this.isLoading = false;
          let errors = err.response.data;
          this.errors = errors;
        });
    }
  },
  created() {
    let formData = window._formData;
    this.name = formData.name;
    this.description = formData.description;
    this.url = formData.url;
    this.min_price = formData.min_price;
    this.max_price = formData.max_price;
    this.quantity = formData.quantity;
    this.unit_of_measurement = formData.unit_of_measurement;
    this.start_from = formData.start_from;
    this.contract_type = formData.contract_type;
    this.service_coverage = formData.service_coverage;
    this.additional_requirements = formData.additional_requirements;
  }
});
