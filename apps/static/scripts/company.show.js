import app from "./app.js";
import NativeToast from "native-toast";

export default new Vue({
  methods: {},
  created() {
    if (successVerification != null) {
      NativeToast({
        message:
          successVerification === true
            ? "Email Verified"
            : "Invalid Verification",
        position: "south",
        edge: true,
        icon: true,
        timeout: 5000,
        type: successVerification === true ? "success" : "error"
      });
    }
  }
});
