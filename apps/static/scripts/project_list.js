import Loading from "vue-loading-overlay";
import axios from "axios";
import app from "./app.js";
import VueMoment from "vue-moment";

Vue.use(VueMoment);

export default new Vue({
  el: "#project-list",
  components: {
    loading: Loading
  },
  data() {
    return {
      name: "",
      icon_filepath: "",
      description: "",
      min_price: "",
      max_price: "",
      service_coverage: "",
      unit_of_measurement: "",
      quantity: "",
      contract: "",
      valid_until: "",
      additional_requirements: "",
      about: true,
      id: null,
      isLoading: false,
      offers: [],
      created_at: "",
      closed_at: null,
      status: {
        closable: false
      }
    };
  },
  methods: {
    lookup(id) {
      this.id = id;
      axios.get("/api/request/" + id).then(response => {
        console.log(response);
        this.name = response.data.name;
        this.icon_filepath = response.data.icon_filepath;
        this.description = response.data.description;
        this.min_price = response.data.min_price;
        this.max_price = response.data.max_price;
        this.service_coverage = response.data.service_coverage;
        this.unit_of_measurement = response.data.unit_of_measurement;
        this.quantity = response.data.quantity;
        this.contract = response.data.contract;
        this.valid_until = response.data.valid_until;
        this.additional_requirements = response.data.additional_requirements;
        this.about = true;
        this.created_at = response.data.created_at;
        this.offers = response.data.offer_set;
        this.closed_at = response.data.close_at;

        this.offers.forEach(element => {
          if (element.is_accepted) {
            this.status.closable = true;
          }
        });
      });
    },
    page(pageNumber) {
      if (pageNumber == 1) {
        this.about = true;
      } else {
        this.about = false;
      }
    },
    view(id) {
      window.open("/offer/" + id, "_blank");
    },
    cancelRequest() {
      swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        buttons: [true, "Cancel Request"],
        dangerMode: true
      }).then(result => {
        if (result) {
          this.isLoading = true;
          axios
            .delete("/request/" + this.id + "/delete")
            .then(res => {
              this.isLoading = false;
              swal({
                title: "Request Canceled",
                text: "",
                icon: "success",
                closeOnClickOutside: false
              }).then(() => {
                location.reload();
              });
            })
            .catch(err => {
              this.isLoading = false;
              console.error(err.response);
              if (err.response.status == 430) {
                swal({
                  icon: "error",
                  title: "Not Permitted",
                  text: err.response.data.message,
                  closeOnClickOutside: false
                });
              }
            });
        }
      });
    },
    acceptOffer(id) {
      this.isLoading = true;
      axios
        .put("/offer/" + id + "/status", { status: 3 })
        .then(res => {
          this.isLoading = false;
          swal({
            title: "Offer Accepted",
            icon: "success",
            closeOnClickOutside: false
          }).then(() => {
            location.reload();
          });
        })
        .catch(err => {
          this.isLoading = false;
          console.log(err);
          if (err.response.status == 430) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
        });
    },
    rejectOffer(id) {
      this.isLoading = true;
      axios
        .put("/offer/" + id + "/status", { status: 5 })
        .then(res => {
          this.isLoading = false;
          swal({
            title: "Offer Rejected",
            icon: "success",
            closeOnClickOutside: false
          }).then(() => {
            location.reload();
          });
        })
        .catch(err => {
          this.isLoading = false;
          console.log(err);
          if (err.response.status == 430) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
        });
    },
    closeRequest() {
      swal({
        icon: "warning",
        title: "Are you sure?",
        icon: "warning",
        buttons: [true, "Cancel Request"],
        dangerMode: true
      }).then(result => {
        if (result) {
          this.isLoading = true;
          axios
            .put("/api/request/" + this.id + "/close")
            .then(res => {
              console.log(res);
              this.isLoading = false;
              swal({
                icon: "success",
                title: "Offer Closed",
                text: "",
                closeOnClickOutside: false
              }).then(() => {
                location.reload();
              });
            })
            .catch(err => {
              console.log(err);
              this.isLoading = false;
              if (err.response.status == 430) {
                swal({
                  icon: "error",
                  title: "Not Permitted",
                  text: err.response.data.message,
                  closeOnClickOutside: false
                });
              }
            });
        }
      });
    }
  }
});
