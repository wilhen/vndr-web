import Loading from "vue-loading-overlay";
import axios from "axios";
import app from "./app.js";

export default new Vue({
  el: "#create_request_form",
  components: {
    loading: Loading
  },
  data() {
    return {
      name: "",
      description: "",
      url: "",
      category: "",
      min_price: "",
      max_price: "",
      quantity: "",
      unit_of_measurement: "",
      start_from: "",
      contract_type: 1,
      service_coverage: "",
      additional_requirements: "",
      errors: {},
      isLoading: false
    };
  },
  methods: {
    submitForm(event) {
      event.preventDefault();
      this.isLoading = true;
      axios
        .post("/request/store", {
          name: this.name,
          description: this.description,
          url: this.url,
          min_price: this.min_price ? this.min_price : null,
          max_price: this.max_price,
          quantity: this.quantity ? this.quantity : null,
          unit_of_measurement: this.unit_of_measurement,
          start_from: this.start_from ? this.start_from : null,
          contract_type: this.contract_type,
          service_coverage: this.service_coverage,
          additional_requirements: this.additional_requirements
        })
        .then(res => {
          location.href = "/requests";
        })
        .catch(err => {
          console.log(err.response);
          this.isLoading = false;
          let errors = err.response.data;
          this.errors = errors;
        });
    }
  }
});
