import Loading from "vue-loading-overlay";
import axios from "axios";
import app from "./app.js";

export default new Vue({
  el: "#offers",
  components: {
    loading: Loading
  },
  data() {
    return {
      bid_price: "",
      current_bid: "$200",
      offerId: null,
      isLoading: false
    };
  },
  methods: {
    create_version() {
      this.isLoading = true;
      axios
        .post("/offer/" + this.offerId + "/version", {
          bid_price: this.bid_price
        })
        .then(response => {
          console.log(response);
          this.current_bid = "$" + this.bid_price;
          this.isLoading = false;
          document.getElementById("current_bid-" + this.offerId).innerText =
            "$" + this.bid_price;
          document.getElementById("status-" + this.offerId).innerText =
            "Reversioned";
          $("#offer-version-modal").modal("hide");
        })
        .catch(err => {
          this.isLoading = false;
          if (err.response.status == 430) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
          console.error(err);
        });
    },
    addVersionModal(offerId) {
      this.offerId = offerId;
      this.bid_price = "";
      this.current_bid = document.getElementById(
        "current_bid-" + this.offerId
      ).innerText;
    },
    retractOffer(offerId) {
      swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        buttons: [true, "Retract Offer"],
        dangerMode: true
      }).then(result => {
        if (result) {
          this.isLoading = true;
          axios
            .delete("/offer/" + offerId + "/delete")
            .then(res => {
              let data = res.data.message;
              this.isLoading = false;
              swal({
                title: "Offer Retracted!",
                text: "",
                icon: "success",
                closeOnClickOutside: false
              }).then(() => {
                location.href = "/offer";
              });
            })
            .catch(err => {
              this.isLoading = false;
              if (err.response.status == 430) {
                swal({
                  icon: "error",
                  title: "Not Permitted",
                  text: err.response.data.message,
                  closeOnClickOutside: false
                });
              }
              console.error(err);
            });
        }
      });
    }
  },
  mounted() {
    this.bid_price = "";
  }
});
