import Loading from "vue-loading-overlay";
import axios from "axios";
import app from "./app.js";

export default new Vue({
  el: "#offer-side",
  data() {
    return {
      offerId: null,
      current_bid: "$" + bidPrice,
      bid_price: "",
      selectedPage: "offer",
      isLoading: false
    };
  },
  components: {
    loading: Loading
  },
  methods: {
    page(pageNumber) {
      if (pageNumber == 1) {
        this.selectedPage = "offer";
      } else if (pageNumber == 2) {
        this.selectedPage = "version";
      } else {
        this.selectedPage = "discussion";
      }
    },
    retractOffer(offerId) {
      swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        buttons: [true, "Retract Offer"],
        dangerMode: true
      }).then(result => {
        if (result) {
          this.isLoading = true;
          axios
            .delete("/offer/" + offerId + "/delete")
            .then(res => {
              let data = res.data.message;
              this.isLoading = false;
              swal({
                title: "Offer Retracted!",
                text: "",
                icon: "success",
                closeOnClickOutside: false
              }).then(() => {
                location.href = "/offer";
              });
            })
            .catch(err => {
              this.isLoading = false;
              if (err.response.status == 430) {
                swal({
                  icon: "error",
                  title: "Not Permitted",
                  text: err.response.data.message,
                  closeOnClickOutside: false
                });
              }
              console.error(err);
            });
        }
      });
    },
    addVersionModal(offerId) {
      this.offerId = offerId;
    },
    create_version() {
      this.isLoading = true;
      axios
        .post("/offer/" + this.offerId + "/version", {
          bid_price: this.bid_price
        })
        .then(response => {
          console.log(response);
          this.current_bid = "$" + this.bid_price;
          location.reload();
        })
        .catch(err => {
          this.isLoading = false;
          if (err.response.status == 430) {
            swal({
              icon: "error",
              title: "Not Permitted",
              text: err.response.data.message,
              closeOnClickOutside: false
            });
          }
          console.error(err);
        });
    }
  },
  mounted() {}
});
