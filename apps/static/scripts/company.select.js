import axios from "axios";
import app from "./app.js";

export default new Vue({
  el: "#company-select",
  methods: {
    selectCompany(id) {
      console.log(id);
      axios
        .post("/company/" + id + "/change")
        .then(res => {
          console.log(res);
          location.href = "/";
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
});
