from rest_framework import serializers


class ApiLocationSerializer(serializers.Serializer):

    address = serializers.CharField(max_length=191)
    postcode = serializers.IntegerField()
    suburb = serializers.CharField(max_length=191)
    state = serializers.CharField(max_length=191)
    country = serializers.CharField(max_length=191)

    lat = serializers.FloatField(allow_null=True)
    lng = serializers.FloatField(allow_null=True)
