from rest_framework import serializers
from apps.models.project import Project


class ApiProjectSerializer(serializers.Serializer):

    name = serializers.CharField(max_length=191, min_length=10)
    description = serializers.CharField(min_length=25)
    url = serializers.URLField(allow_blank=True)
    min_price = serializers.FloatField(
        allow_null=True)
    max_price = serializers.FloatField()
    quantity = serializers.FloatField(
        allow_null=True)
    unit_of_measurement = serializers.CharField(
        allow_blank=True)

    start_from = serializers.DateField(allow_null=True)

    contract_type = serializers.IntegerField()
    service_coverage = serializers.CharField(allow_blank=True)

    additional_requirements = serializers.CharField(
        allow_blank=True)
