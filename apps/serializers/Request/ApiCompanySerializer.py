from rest_framework import serializers


class ApiCompanySerializer(serializers.Serializer):

    name = serializers.CharField(max_length=191, min_length=3)
    rbn = serializers.CharField(max_length=191, min_length=9)
    abn = serializers.CharField(max_length=11, min_length=11)
    phone = serializers.CharField(max_length=12, min_length=10)
    fax = serializers.CharField(max_length=12, allow_blank=True)
    email = serializers.EmailField()
    website = serializers.URLField(allow_blank=True)
    step = serializers.IntegerField()
