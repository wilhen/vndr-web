from rest_framework import serializers
from apps.models.offer import Offer
from apps.serializers.OfferVersionSerializer import OfferVersionSerializer
from apps.serializers.OfferStatusSerializer import OfferStatusSerializer


class OfferSerializer(serializers.ModelSerializer):

    offerversion_set = OfferVersionSerializer(read_only=True, many=True)
    status = OfferStatusSerializer(read_only=True)

    class Meta:
        model = Offer
        fields = ('id', 'project_id', 'bid_price', 'created_at', 'updated_at', 'accepted_at', 'current_version',
                  'deleted', 'is_accepted', 'offerversion_set', 'status')
