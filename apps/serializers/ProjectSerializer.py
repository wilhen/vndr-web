from rest_framework import serializers
from apps.models.project import Project
from apps.serializers.OfferSerializer import OfferSerializer
from apps.models.contract_type import ContractType


class ProjectSerializer(serializers.ModelSerializer):
    contract = serializers.SlugRelatedField(
        queryset=ContractType.objects.all(), slug_field='contract_type'
    )
    offer_set = OfferSerializer(read_only=True, many=True)

    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'created_at', 'updated_at',
                  'min_price', 'max_price', 'quantity', 'unit_of_measurement',
                  'additional_requirements', 'valid_until', 'location', 'icon_filepath',
                  'service_coverage', 'contract', 'url', 'start_from', 'offer_set', 'close_at')
