from rest_framework import serializers
from apps.models.offer_version import OfferVersion


class OfferVersionSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfferVersion
        fields = ('id', 'bid_price', 'created_at', 'updated_at',
                  'deleted')
