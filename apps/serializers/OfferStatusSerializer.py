from rest_framework import serializers
from apps.models.offer_status import OfferStatus


class OfferStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfferStatus
        fields = ('id', 'status', 'created_at', 'updated_at', 'deleted')
