from django.forms import ModelForm
from apps.models.offer_version import OfferVersion

class OfferVersionForm(ModelForm):
    class Meta:
        model = OfferVersion
        fields = [ 'user_id', 'bid_price']
