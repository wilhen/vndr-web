from django import forms


class ProjectForm(forms.Form):

    name = forms.CharField(max_length=191, min_length=10)
    description = forms.CharField(min_length=25)
    url = forms.URLField(required=False)

    min_price = forms.FloatField(required=False)
    max_price = forms.FloatField()

    quantity = forms.FloatField(required=False)
    unit_of_measurement = forms.CharField(required=False)

    start_from = forms.DateField(required=False)

    contract_type = forms.IntegerField()
    service_coverage = forms.CharField(required=False)

    additional_requirements = forms.CharField(required=False)
