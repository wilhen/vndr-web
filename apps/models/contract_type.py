from django.db import models


class ContractType(models.Model):
    contract_type = models.CharField(max_length=191)

    class Meta:
        db_table = 'contract_types'
