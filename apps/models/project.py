from django.db import models
from apps.models.contract_type import ContractType
from safedelete.models import SafeDeleteModel, SOFT_DELETE, SOFT_DELETE_CASCADE


class Project(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=191)
    description = models.TextField()
    additional_requirements = models.TextField(null=True)
    valid_until = models.DateField(null=True)
    start_from = models.DateField(null=True)

    project_poster_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)
    close_at = models.DateTimeField(null=True)
    location = models.CharField(max_length=191)
    service_coverage = models.CharField(max_length=191, null=True)

    contract = models.OneToOneField(
        ContractType,
        on_delete=models.CASCADE
    )

    url = models.URLField()
    min_price = models.FloatField(null=True)
    max_price = models.FloatField()
    quantity = models.FloatField(null=True)
    unit_of_measurement = models.CharField(null=True, max_length=191)

    icon_filepath = models.CharField(max_length=191, null=True)

    class Meta:
        db_table = 'projects'
