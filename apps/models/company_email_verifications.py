from django.db import models
from safedelete.models import SafeDeleteModel, SOFT_DELETE


class CompanyEmailVerification(SafeDeleteModel):

    id = models.AutoField(primary_key=True)
    verification_id = models.CharField(max_length=191)
    is_accepted = models.BooleanField(default=False)
    company_id = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)

    class Meta:
        db_table = 'company_email_verifications'
