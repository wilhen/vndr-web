from django.db import models
from safedelete.models import SafeDeleteModel, SOFT_DELETE
from django.contrib.auth.models import User


class Company(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    id = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length=191)
    registered_business_name = models.CharField(max_length=191)
    phone_number = models.CharField(max_length=12)
    fax_number = models.CharField(max_length=12, null=True)
    business_email = models.CharField(max_length=191)
    address = models.CharField(max_length=191)
    postcode = models.IntegerField()
    abn = models.IntegerField()

    suburb = models.CharField(max_length=191)
    state = models.CharField(max_length=191)
    country = models.CharField(max_length=191)
    company_logo = models.CharField(max_length=191)
    lat = models.FloatField(null=True)
    lng = models.FloatField(null=True)
    website = models.URLField()
    is_verified = models.BooleanField(default=False)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='companies')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)

    class Meta:
        db_table = 'companies'
