from django.db import models
from apps.models.offer import Offer
from safedelete.models import SafeDeleteModel, SOFT_DELETE


class OfferVersion(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    id = models.AutoField(primary_key=True)
    bid_price = models.FloatField()
    offer = models.ForeignKey(
        Offer, on_delete=models.CASCADE)
    user_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)

    class Meta:
        db_table = 'offer_versions'
