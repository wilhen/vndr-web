from django.db import models
from apps.models.project import Project
from safedelete.models import SafeDeleteModel, SOFT_DELETE, SOFT_DELETE_CASCADE
from apps.models.offer_status import OfferStatus


class Offer(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(to=Project, on_delete=models.CASCADE)
    bid_price = models.FloatField()
    is_accepted = models.BooleanField(default=False)
    accepted_at = models.DateTimeField(null=True)
    current_version = models.IntegerField(null=True)
    status = models.ForeignKey(
        OfferStatus, on_delete=models.CASCADE
    )
    user_id = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)

    class Meta:
        db_table = 'offers'
