from django.db import models


class OfferStatus(models.Model):
    status = models.CharField(max_length=191)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)

    class Meta:
        db_table = 'offer_statuses'
