
import redis
from django.conf import settings
from django.shortcuts import redirect, reverse
from django.urls import resolve


class SelectedCompanyMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

        self.red = redis.StrictRedis(
            host=settings.REDIS_HOST, port=settings.REDIS_PORT)

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        response = self.get_response(request)
        if resolve(request.path_info).url_name == 'company.change':
            return response

        if not self.process_request(request):
            return redirect(reverse('company.change'))

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_request(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        # Code to be executed for each request/response after
        # the view is called.
        if request.user.is_authenticated:
            if not self.red.get('company_' + str(request.user.id)) and not request.session.get('company_id', None):
                return False

        return request
