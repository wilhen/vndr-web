"""UXD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url

urlpatterns = [
    path('', include('apps.urls.requests')),
    path('offer/', include('apps.urls.offers')),
    path('api/', include('apps.urls.apis')),
    path('admin/', admin.site.urls),  # admin
    path('company/', include('apps.urls.companies')),  # admin
    url(r'^', include(('django.contrib.auth.urls', 'apps'), namespace='auth')),
    url(r'^', include(('social_django.urls', 'apps'), namespace='social')),
    # path('api-auth/', include('rest_framework.urls'))
]
